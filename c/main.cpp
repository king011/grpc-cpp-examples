#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <grpc/grpc.h>
#include <grpc++/channel.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/security/credentials.h>

#include "city/zoo/zoo.grpc.pb.h"

#define LAddr "localhost:6000"
void show_status(grpc::Status status);
void list(city::zoo::Server::Stub *stub);
void hunt(city::zoo::Server::Stub *stub);
void hunt2(city::zoo::Server::Stub *stub);
void eat(city::zoo::Server::Stub *stub);
void input_name_num(std::string &name, google::protobuf::uint32 &num);
int main(int argc, char *argv[])
{
    //創建 客戶端
    std::unique_ptr<city::zoo::Server::Stub> stub = city::zoo::Server::NewStub(
        grpc::CreateChannel(
            LAddr,
            grpc::InsecureChannelCredentials()));

    std::string cmd;
    while (true)
    {
        std::cout << "select action $>";
        std::cin >> cmd;
        if (cmd == "q")
        {
            break;
        }
        else if (cmd == "l")
        {
            list(stub.get());
        }
        else if (cmd == "h")
        {
            hunt(stub.get());
        }
        else if (cmd == "h2")
        {
            hunt2(stub.get());
        }
        else if (cmd == "e")
        {
            eat(stub.get());
        }
        else
        {
            puts("q  -> quit");
            puts("l  -> list zoo info [request response.stream]");
            puts("h  -> hunt [request response]");
            puts("h2  -> hunt2 [request.stream response]");
            puts("e  -> eat [request.stream response.stream]");
        }
    }
    return 0;
}

void show_status(grpc::Status status)
{
    std::string msg = status.error_message();
    switch (status.error_code())
    {
    case grpc::StatusCode::OK:
        puts("OK");
        break;
    case grpc::StatusCode::CANCELLED:
        std::cout << "err : [Canceled] " << msg << "\n";
        break;
    case grpc::StatusCode::UNKNOWN:
        std::cout << "err : [Unknown] " << msg << "\n";
        break;
    case grpc::StatusCode::INVALID_ARGUMENT:
        std::cout << "err : [InvalidArgument] " << msg << "\n";
        break;
    case grpc::StatusCode::DEADLINE_EXCEEDED:
        std::cout << "err : [DeadlineExceeded] " << msg << "\n";
        break;
    case grpc::StatusCode::NOT_FOUND:
        std::cout << "err : [NotFound] " << msg << "\n";
        break;
    case grpc::StatusCode::ALREADY_EXISTS:
        std::cout << "err : [AlreadyExists] " << msg << "\n";
        break;
    case grpc::StatusCode::PERMISSION_DENIED:
        std::cout << "err : [PermissionDenied] " << msg << "\n";
        break;
    case grpc::StatusCode::RESOURCE_EXHAUSTED:
        std::cout << "err : [ResourceExhausted] " << msg << "\n";
        break;
    case grpc::StatusCode::FAILED_PRECONDITION:
        std::cout << "err : [FailedPrecondition] " << msg << "\n";
        break;
    case grpc::StatusCode::ABORTED:
        std::cout << "err : [Aborted] " << msg << "\n";
        break;
    case grpc::StatusCode::OUT_OF_RANGE:
        std::cout << "err : [OutOfRange] " << msg << "\n";
        break;
    case grpc::StatusCode::UNIMPLEMENTED:
        std::cout << "err : [Unimplemented] " << msg << "\n";
        break;
    case grpc::StatusCode::INTERNAL:
        std::cout << "err : [Internal] " << msg << "\n";
        break;
    case grpc::StatusCode::UNAVAILABLE:
        std::cout << "err : [Unavailable] " << msg << "\n";
        break;
    case grpc::StatusCode::DATA_LOSS:
        std::cout << "err : [DataLoss] " << msg << "\n";
        break;
    case grpc::StatusCode::UNAUTHENTICATED:
        std::cout << "err : [Unauthenticated] " << msg << "\n";
        break;
    default:
        std::cout << "err : [unknown status] " << msg << "\n";
        break;
    }
}
void list(city::zoo::Server::Stub *stub)
{
    puts("test list [request response.stream]");
    grpc::ClientContext context;
    city::zoo::ListRequest request;
    auto reader = stub->List(&context, request);
    city::zoo::ListResponse response;
    while (reader->Read(&response))
    {
        if (response.has_node())
        {
            const auto &node = response.node();
            std::cout << node.name() << " " << node.num() << "\n";
        }
    }
    grpc::Status status = reader->Finish();
    show_status(status);
}
void input_name_num(std::string &name, google::protobuf::uint32 &num)
{
    std::string str;
    while (true)
    {
        std::cout << "input name $> ";
        std::cin >> str;
        boost::trim(str);
        if (!str.empty())
        {
            if (str == "e")
            {
                return;
            }
            name = str;
            break;
        }
    }
    while (true)
    {
        std::cout << "input num  $> ";
        std::cin >> str;
        boost::trim(str);
        if (!str.empty())
        {
            try
            {
                num = boost::lexical_cast<google::protobuf::uint32>(str);
                if (num > 0)
                {
                    break;
                }
            }
            catch (const boost::bad_lexical_cast &)
            {
            }
        }
    }
}
void hunt(city::zoo::Server::Stub *stub)
{
    puts("test hunt [request response]");

    std::string name, str;
    google::protobuf::uint32 num;
    input_name_num(name, num);
    if (name.empty())
    {
        puts("cancel");
        return;
    }

    grpc::ClientContext context;

    // 創建 request
    city::zoo::Animal *animal = new city::zoo::Animal();
    animal->set_name(name);
    animal->set_num(num);

    city::zoo::HuntRequest request;
    request.set_allocated_node(animal);

    // 創建 response
    city::zoo::HuntResponse response;
    grpc::Status status = stub->Hunt(&context, request, &response);
    show_status(status);
}
void hunt2(city::zoo::Server::Stub *stub)
{
    puts("test hunt2 [request.stream response]");
    grpc::ClientContext context;
    bool cancel = true;
    city::zoo::Hunt2Response response;
    auto writer = stub->Hunt2(&context, &response);
    while (true)
    {
        std::string name, str;
        google::protobuf::uint32 num;
        input_name_num(name, num);
        if (name.empty())
        {
            if (cancel)
            {
                context.TryCancel();
                break;
            }
            // 通知 服務器 write 結束
            writer->WritesDone();
            break;
        }
        cancel = false;

        // 創建 request
        city::zoo::Animal *animal = new city::zoo::Animal();
        animal->set_name(name);
        animal->set_num(num);

        city::zoo::Hunt2Request request;
        request.set_allocated_node(animal);

        // 發送 請求
        if (!writer->Write(request))
        {
            break;
        }
        puts("Write ok");
    }

    // 狀態碼
    grpc::Status status = writer->Finish();
    if (status.ok())
    {
        std::cout << "add num " << response.num() << "\n";
    }
    else
    {
        show_status(status);
    }
}
void eat(city::zoo::Server::Stub *stub)
{
    puts("test eat [request.stream response.stream]");
    grpc::ClientContext context;
    auto stream = stub->Eat(&context);
    std::string str;

    city::zoo::EatRequest request;
    city::zoo::EatResponse response;
    while (true)
    {
        std::cout << "eat something ? yes or no $> ";
        std::cin >> str;
        if (str == "y" || str == "yes" || str == "true" || str == "1")
        {
            if (!stream->Write(request))
            {
                puts("write error");
                break;
            }

            if (!stream->Read(&response))
            {
                puts("read error");
                break;
            }
            if (response.has_node())
            {
                auto node = response.node();
                std::cout << "eat " << node.name() << " " << node.num() << "\n";
            }
        }
        else
        {
            stream->WritesDone();
            break;
        }
    }
    grpc::Status status = stream->Finish();
    show_status(status);
}
