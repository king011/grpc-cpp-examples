#include <boost/thread.hpp>
#include <boost/unordered_map.hpp>
#include <boost/algorithm/string.hpp>

#include <grpc/grpc.h>
#include <grpc++/server.h>
#include <grpc++/server_builder.h>
#include <grpc++/server_context.h>
#include <grpc++/security/server_credentials.h>

#include "city/zoo/zoo.grpc.pb.h"

#define LAddr "localhost:6000"
// 物種 上限
#define AnimalSpeciesLimit 5
// 單個物種 數量 上限
#define AnimalNumLimit 10

class Zoo final : public city::zoo::Server::Service
{
  private:
    //實現 服務器 List 接口
    virtual grpc::Status List(
        grpc::ServerContext *context,
        const city::zoo::ListRequest *request,
        grpc::ServerWriter<city::zoo::ListResponse> *writer) override
    {
        // 創建 數據 快照
        std::vector<city::zoo::Animal> animals;
        {
            boost::mutex::scoped_lock lock(_mutex);
            if (_animals.empty()) // 沒有 動物 直接返回
            {
                return grpc::Status::OK;
            }
            // 創建快照 以便 解鎖
            std::vector<city::zoo::Animal> tmps(_animals.size());
            int i = 0;
            for (const auto node : _animals)
            {
                tmps[i++] = node.second;
            }
            animals.swap(tmps);
        }

        // 以 流 返回 數據
        for (const auto &animal : _animals)
        {
            // 是否 已經 取消 請求
            if (context->IsCancelled())
            {
                return grpc::Status::CANCELLED;
            }

            // 返回 數據
            city::zoo::ListResponse response;
            response.set_allocated_node(new city::zoo::Animal(animal.second));
            if (!writer->Write(response))
            {
                return grpc::Status(grpc::StatusCode::UNKNOWN, "writer closed");
            }
        }
        // 返回 成功
        return grpc::Status::OK;
    }
    //實現 服務器 Hunt 接口
    virtual grpc::Status Hunt(
        grpc::ServerContext *context,
        const city::zoo::HuntRequest *request,
        city::zoo::HuntResponse *response) override
    {
        try
        {
            // 驗證 參數
            const city::zoo::Animal &animal = request->node();
            std::string name = animal.name();
            boost::trim(name);
            if (name.empty())
            {
                throw grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "name can't be empty");
            }
            google::protobuf::uint32 num = animal.num();
            if (num < 1)
            {
                throw grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "num must larger than 0");
            }

            // 邏輯 處理
            std::cout << "hunt " << name << " " << num << "\n";
            hunt(name, num);

            // 設置 響應結果
        }
        catch (const std::string &e) // 返回 錯誤
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, e);
        }
        catch (const grpc::Status &status) // 返回 錯誤
        {
            return status;
        }
        // 返回 成功
        return grpc::Status::OK;
    }
    //實現 服務器 Hunt2 接口
    virtual grpc::Status Hunt2(
        grpc::ServerContext *context,
        grpc::ServerReader<city::zoo::Hunt2Request> *reader,
        city::zoo::Hunt2Response *response) override
    {
        try
        {
            google::protobuf::uint32 sum = 0;
            city::zoo::Hunt2Request request;
            while (reader->Read(&request))
            {
                if (context->IsCancelled())
                {
                    std::cout << "Hunt2 Cancelled\n";
                    throw grpc::Status::CANCELLED;
                }
                if (request.has_node())
                {
                    // 驗證 參數
                    const city::zoo::Animal &animal = request.node();
                    std::string name = animal.name();
                    boost::trim(name);
                    if (name.empty())
                    {
                        throw grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "name can't be empty");
                    }
                    google::protobuf::uint32 num = animal.num();
                    if (num < 1)
                    {
                        throw grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "num must larger than 0");
                    }

                    // 執行 添加
                    std::cout << "hunt2 " << name << " " << num << "\n";
                    hunt(name, num);
                    sum += num;
                }
            }
            if (context->IsCancelled())
            {
                throw grpc::Status::CANCELLED;
            }
            // 設置 返回值
            response->set_num(sum);
        }
        catch (const std::string &e) // 返回 錯誤
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, e);
        }
        catch (const grpc::Status &status) // 返回 錯誤
        {
            return status;
        }

        // 返回 成功
        return grpc::Status::OK;
    }
    virtual grpc::Status Eat(
        grpc::ServerContext *context,
        grpc::ServerReaderWriter<city::zoo::EatResponse, city::zoo::EatRequest> *stream) override
    {
        try
        {
            city::zoo::EatRequest request;
            city::zoo::EatResponse response;
            while (true)
            {
                // 驗證 取消
                if (context->IsCancelled())
                {
                    return grpc::Status::CANCELLED;
                }

                // 獲得 請求
                if (!stream->Read(&request))
                {
                    // 客戶端 沒有新 請求 返回
                    break;
                }

                // 驗證 取消
                if (context->IsCancelled())
                {
                    return grpc::Status::CANCELLED;
                }

                // 執行 邏輯
                std::string name;
                google::protobuf::uint32 num;
                eat(name, num);

                // 驗證 取消
                if (context->IsCancelled())
                {
                    return grpc::Status::CANCELLED;
                }

                // 返回 響應
                city::zoo::Animal *animal = new city::zoo::Animal();
                animal->set_name(name);
                animal->set_num(num);
                std::cout << "eat " << name << " " << num << "\n";

                response.set_allocated_node(animal);
                if (!stream->Write(response))
                {
                    return grpc::Status(grpc::StatusCode::UNKNOWN, "write eror");
                }
            }
        }
        catch (const std::string &e) // 返回 錯誤
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, e);
        }
        catch (const grpc::Status &status) // 返回 錯誤
        {
            return status;
        }

        // 返回 成功
        return grpc::Status::OK;
    }

    // 模擬 數據 存儲
    boost::mutex _mutex;
    boost::unordered_map<std::string, city::zoo::Animal> _animals;

  public:
    Zoo()
    {
        city::zoo::Animal cat;
        cat.set_name("fish");
        cat.set_num(5);
        _animals[cat.name()] = cat;
    }
    void hunt(const std::string &name, google::protobuf::uint32 num)
    {
        if (num > AnimalNumLimit)
        {
            throw std::string("Animal count reach the upper limit");
        }

        boost::mutex::scoped_lock lock(_mutex);
        auto find = _animals.find(name);
        if (find == _animals.end())
        {
            if (_animals.size() == AnimalSpeciesLimit)
            {
                throw std::string("Animal species reach the upper limit");
            }
            city::zoo::Animal animal;
            animal.set_name(name);
            animal.set_num(num);
            _animals[name] = animal;
        }
        else
        {
            num += find->second.num();
            if (num >= AnimalNumLimit)
            {
                throw std::string("Animal count reach the upper limit");
            }
            find->second.set_num(num);
        }
    }
    void eat(std::string &name, google::protobuf::uint32 &num)
    {
        boost::mutex::scoped_lock lock(_mutex);
        if (_animals.empty())
        {
            throw std::string("zoo empty");
        }
        auto node = _animals.begin();
        name = node->first;
        auto sum = node->second.num();
        if (sum > 3)
        {
            sum -= 3;
            num = 3;
            node->second.set_num(sum);
        }
        else
        {
            num = sum;
            _animals.erase(node);
        }
    }
};

int main(int argc, char *argv[])
{
    // 創建一個 grpc 服務器
    grpc::ServerBuilder builder;
    builder.AddListeningPort(
        LAddr,
        grpc::InsecureServerCredentials() // h2c
    );

    // 創建 服務 實例
    Zoo zoo;

    // 註冊 rpc 服務
    builder.RegisterService(&zoo);

    //運行 服務
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << LAddr << std::endl;

    //等待服務 停止
    server->Wait();
    return 0;
}