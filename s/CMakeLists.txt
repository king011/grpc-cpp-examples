cmake_minimum_required(VERSION 3.0)

#set(CMAKE_VERBOSE_MAKEFILE ON) 
set(CMAKE_MODULE_PATH 
    "${CMAKE_SOURCE_DIR}/cmake"
    "${CMAKE_SOURCE_DIR}/cmake/modules"
)
include(cotire)

# 設置 編譯參數
add_compile_options(-std=gnu++11)

# 定義 項目
set(target_name s)
set(target_definitions -DUNICODE)
set(target_sources)
set(target_headers "${CMAKE_SOURCE_DIR}/..")
set(target_libs)
if(WIN32)
    list(APPEND target_definitions 
        -DWIN32
        -DWIN32_LEAN_AND_MEAN
        -D_WIN32_WINNT=0x0601
    )
    list(APPEND target_libs
        stdc++
        #-lws2_32
        #-lwsock32
    )
endif()


if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    list(APPEND target_definitions -D_DEBUG)
endif()

project(${target_name})


# 加載子腳本
include(find_depend)
include(source)

# 定義 宏
add_definitions(${target_definitions})

# 編譯 項目
include_directories(${target_headers})
add_executable(${target_name} ${target_sources})
cotire(${target_name})
target_link_libraries(${target_name} ${target_libs})
