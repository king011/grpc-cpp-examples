# Boost
find_package(Boost
    1.58.0
    REQUIRED
    COMPONENTS 
        date_time
        thread
)
list(APPEND target_headers ${Boost_INCLUDE_DIRS})
list(APPEND target_libs ${Boost_LIBRARIES})


find_package(Protobuf REQUIRED)
list(APPEND target_headers ${Protobuf_INCLUDE_DIRS})
list(APPEND target_libs ${Protobuf_LIBRARIES})


find_package(GRPC
    REQUIRED
)
list(APPEND target_headers ${GRPC_INCLUDE_DIRS})
list(APPEND target_libs ${GRPC_LIBRARIES})



# 刪除 重複項
if(target_headers)
    list(REMOVE_DUPLICATES target_headers)
endif()
if(target_libs)
    list(REMOVE_DUPLICATES target_libs)
endif()
